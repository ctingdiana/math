# math

这是一个参考网络与张宇数学36讲整理的考研数学资料。

如果发现其中有公式等错误，可以在issue告诉一下我，谢谢捏

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/starlightOvO/math.git
git branch -M main
git push -uf origin main
```

## 安装该资料

### git安装

git clone git@gitlab.com:starlightOvO/math.git

### 一般安装

选择clone左边的下载按钮,下载相应的zip文件,解压zip即可.

## 浏览该资料

直接类似于网盘点入文件夹查看即可.

## 目录

### 这是一个目录    

#### 1.高等数学

1.1 基础知识

1.2 极限与连续

1.3 一元函数微分学的概念与计算

1.4 一元函数微分学的几何应用

1.5 中值定理

1.6 零点问题，微分不等式

1.7 一元函数积分学的概念与计算

1.8 一元函数积分学的应用

1.9 一元函数积分学的综合应用

1.10 多元函数微分学

1.11 二重积分

1.12 常微分方程

1.13 无穷级数

1.14 专题应用

1.X 一些简单的积分技巧

#### 2.线性代数

2.1 行列式

2.2 行列式的综合计算与应用

2.3 矩阵的基本概念与运算

2.4 伴随矩阵，初等矩阵与矩阵方程 

2.5 向量

2.6 线性方程组

2.7 特征值与特征向量


#### 3.概率论与数理统计

3.1 随机事件与概率

3.2 一维随机变量及其分布

3.3 一维随机变量函数的分布

3.4 多维随机变量及其分布

3.5 多维随机变量函数的分布

3.6 数字特征

3.7 大数定律与中心极限定理

3.8 统计量及其分布

3.9 参数估计与假设检验